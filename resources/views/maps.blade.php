<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <style>
        body {
          margin: 0;
          padding: 0;
        }
  
        #map {
          position: relative;
          top: 0;
          bottom: 0;
          width: 100%;
          height: 300px;
        }
        #map1 {
          position: relative;
          top: 0;
          bottom: 0;
          width: 100%;
          height: 300px;
        }
        .marker {
        background-image: url('{{ asset("icon/picture1.png") }}');
        background-size: cover;
        width: 50px;
        height: 50px;
        border-radius: 50%;
        cursor: pointer;
      }
      </style>
      <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
</head>
<body>
    <input type="text" id="keyword" > <button type="button" class="btn-marker" data-id="123" onclick="getmaps()"> Cari</button>
    <p id="text-keyword"></p>
    <br>
    <div id="table-marker">
        <p id="text-result"></p>

    </div>
    <p id="text-cordinates"></p>
    <div id="map" ></div>
    <div id="map1" ></div>
    <input type="text" id="lat">
    <input type="text" id="long">
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            ...
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary">Save changes</button>
          </div>
        </div>
      </div>
    </div>
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    {{-- <script src="https://code.jquery.com/jquery-1.12.0.min.js"></script> --}}
<script src='https://api.tiles.mapbox.com/mapbox-gl-js/v2.6.0/mapbox-gl.js'></script>
<link href='https://api.tiles.mapbox.com/mapbox-gl-js/v2.6.0/mapbox-gl.css' rel='stylesheet' />
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCMpVloajVzTS_WGKUnZr3KHh4XxLK28Pw&libraries=places&callback=initMap" async defer></script>
{{-- <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCHCMIlAFaPg_tDO-v73pDTZCuTC3LYfEw&callback=initMap"></script> --}}
<script type="text/javascript">
function getmaps(){
    let keyword = $('#keyword').val();
    let url = "https://api.mapbox.com/geocoding/v5/mapbox.places/"+keyword+".json?access_token=pk.eyJ1IjoiaGF5cmVuIiwiYSI6ImNrNjI4ZWkyYjBhaHUzZG8wNHIydnU2M2QifQ.wGG3GZut3cHYlrt0z8Kw9Q";
    $('#text-keyword').text(keyword);
    console.log(url);
    $.getJSON(url, function(result){
      $.each(result.features, function(i, features){
        let html = "";
         html += "<button type='button' class='btn-marker' data-long='"+features.center[0]+"' data-lat='"+features.center[1]+"' data-center='"+features.center+"'>"+features.place_name+"</button>"
         html += "<br>"
        //  html += "<p>kordinat latitude :"+features.center[0]+"</p>"
        //  html += "<p>kordinat long :"+features.center[1]+"</p>"
        $("#text-result").append(html);
        // $("#text-result").append(features.place_name + "<br> <br>");
        // $("#text-cordinates").append(features.center + "<br> <br>");
      });
      var geojson = result;
      console.log(result.features[0].center[0]);
      const map = new mapboxgl.Map({
        container: 'map',
        style: 'mapbox://styles/mapbox/light-v10',
        center: [result.features[0].center[0], result.features[0].center[1]],
        zoom: 3
        });

        // add markers to map
        // for (const feature of geojson.features) {
        // create a HTML element for each feature
        const el = document.createElement('div');
        el.className = 'marker';

        // make a marker for each feature and add it to the map
        new mapboxgl.Marker(el)
            .setLngLat(geojson.features[0].center)
            .addTo(map);
        // }
    });
    
}
function initMap() {
    var z=document.getElementById("lokasi");
    var map = new google.maps.Map(document.getElementById('map'), {
      center: {lat: -2.548926, lng: 118.0148634},
      zoom: 13,
      mapTypeId:google.maps.MapTypeId.ROADMAP,
      mapTypeControl:false,
      navigationControlOptions:{style:google.maps.NavigationControlStyle.SMALL}
    });
}
mapboxgl.accessToken = 'pk.eyJ1IjoiaGF5cmVuIiwiYSI6ImNrNjI4ZWkyYjBhaHUzZG8wNHIydnU2M2QifQ.wGG3GZut3cHYlrt0z8Kw9Q';

const geojson = {
  'type': 'FeatureCollection',
  'features': [
    {
      'type': 'Feature',
      'geometry': {
        'type': 'Point',
        'coordinates': [110.41842358187012, -7.74997460515398]
      },
      'properties': {
        'title': 'Maguwo',
        'description': 'Washington, D.C. <a type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#exampleModal">asd</a>'
      }
    },
    {
      'type': 'Feature',
      'geometry': {
        'type': 'Point',
        'coordinates': [-122.414, 37.776]
      },
      'properties': {
        'title': 'Pom',
        'description': 'San Francisco, California'
      }
    }
  ]
};

const map = new mapboxgl.Map({
  container: 'map',
  style: 'mapbox://styles/mapbox/light-v10',
  center: [110.41773693636061, -7.751143995756703],
  zoom: 15
});
map.on('style.load', function() {
  
  map.on('click', function(e) {
    var coordinates = e.lngLat;
    console.log(coordinates);
    const el = document.createElement('div');
    el.className = 'marker';
    var marker = new mapboxgl.Marker(el)
      .setLngLat(coordinates)
      .addTo(map);
      document.getElementById("lat").value = coordinates.lat;
      document.getElementById("long").value = coordinates.lng;
  });
});

// add markers to map
for (const feature of geojson.features) {
  // create a HTML element for each feature
  const el = document.createElement('div');
  el.className = 'marker';

  // make a marker for each feature and add it to the map
  new mapboxgl.Marker(el)
    .setLngLat(feature.geometry.coordinates)
    .setPopup(
      new mapboxgl.Popup({ offset: 25 }) // add popups
        .setHTML(
          `<h3>${feature.properties.title}</h3><p>${feature.properties.description}</p>`
        )
    )
    .addTo(map);
}

$("#table-marker").on('click', '.btn-marker', function () {
    var long = $(this).attr('data-long');
    var lat = $(this).attr('data-lat');
    var center = $(this).attr('data-center');
    console.log(long);
    console.log(lat);
    console.log(center);
    const map = new mapboxgl.Map({
        container: 'map',
        style: 'mapbox://styles/mapbox/light-v10',
        center: [long,lat],
        zoom: 15,
    });

    // add markers to map
    // for (const feature of geojson.features) {
    // create a HTML element for each feature
    const el = document.createElement('div');
    el.className = 'marker';

    // make a marker for each feature and add it to the map
    new mapboxgl.Marker(el).setLngLat([long,lat]).addTo(map);

    var geocoder = new google.maps.Geocoder;
    var infowindow = new google.maps.InfoWindow({
        maxWidth: 300,
    });

    //Set Content of Info Window, with HTML Tag
    var infowindowContent = document.getElementById('infowindow-content');

    var asal = new google.maps.LatLng(lat,long);
    // var asal = new google.maps.LatLng(-8.5830695,116.3202515);
    var mapOptions = {
        zoom : 15,
        center : asal,
    }
    var map1 = new google.maps.Map(document.getElementById('map1'), mapOptions);
    var marker = new google.maps.Marker({
        position: asal,
        map: map1,
        draggable: true
    });
        

});
var lat = "-122.414";
var long = "37.776";
function initMap() {
    var geocoder = new google.maps.Geocoder;
    var infowindow = new google.maps.InfoWindow({
        maxWidth: 300,
    });

    //Set Content of Info Window, with HTML Tag
    var infowindowContent = document.getElementById('infowindow-content');

    var asal = new google.maps.LatLng(lat,long);
    // var asal = new google.maps.LatLng(-8.5830695,116.3202515);
    var mapOptions = {
        zoom : 10,
        center : asal,
    }
    var map = new google.maps.Map(document.getElementById('map1'), mapOptions);
    var marker = new google.maps.Marker({
        position: asal,
        map: map,
        draggable: true
    });

    //Hide or Show InfoWindow by Click the Marker
    marker.addListener('click', function() {
        if (isInfoWindowOpen(infowindow)){
            infowindow.close(map, marker);
        } else {
            infowindow.open(map, marker);
        }
    });

    //Set Marker and center of map
    geocoder.geocode({'latLng': asal }, function(results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
            if (results[0]) {
                //set value of InfoWindow
                infowindowContent.children['place-name'].textContent = results[0].address_components[0]['long_name'];
                infowindowContent.children['place-id'].textContent = results[0].place_id;
                infowindowContent.children['place-address'].textContent = results[0].formatted_address;

                $('#latitude,#longitude').show();
                $('#alamat_lokasi').val(results[0].formatted_address);
                $('#latitude').val(marker.getPosition().lat());
                $('#longitude').val(marker.getPosition().lng());
                infowindow.setContent(infowindowContent);
                console.log(results[0]);
                infowindow.open(map, marker);
            }
        }
    });

    // Close InfoWindow when moving the marker, and Open InfoWindow when Drop the Marker
    google.maps.event.addListener(marker, 'drag', function() {
        infowindow.close();
    });
    google.maps.event.addListener(marker, 'dragend', function() {
        //update lat long when drop the marker
        geocoder.geocode({'latLng': marker.getPosition()}, function(results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                if (results[0]) {
                    infowindowContent.children['place-name'].textContent = results[0].address_components[0]['long_name'];
                    infowindowContent.children['place-id'].textContent = results[0].place_id;
                    infowindowContent.children['place-address'].textContent = results[0].formatted_address;

                    $('#latitude,#longitude').show();
                    $('#alamat_lokasi').val(results[0].formatted_address);
                    $('#latitude').val(marker.getPosition().lat());
                    $('#longitude').val(marker.getPosition().lng());
                    infowindow.setContent(infowindowContent);
                    infowindow.open(map, marker);
                    console.log(results[0]);

                    validasiUlang('alamat_lokasi');
                }
            }
        });
    });

}

</script>
</body>
</html>